class Tweet < ApplicationRecord
  paginates_per 5
  validates :message, length: { maximum: 140 }, presence: true
end
